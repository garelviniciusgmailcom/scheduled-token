# TaskServer - UPDATE BASE USEBENS
  * Este projeto foi desenvolvido em java 8, ultilizando Spring Boot Framework versão 2.1.3.RELEASE 

## REQUIREMENTS:
   ### DataBases: 
       * MongoDB 3.6.2 port: 27017
       * Java 8
       * Maven 3.6.2
           
## FEATURES   
	### SCHENDULE
	    - REALIZA ATUALIZAÇÃO DA BASE USEBENS TODOS OS DIAS AS 10H DA MANHÃ
	   	           
## RUN PRODUCTION
	
	### MAVEN COMMAND BUILD
	   * mvn clean install
	   
	### MAVEN COMMAND BUILD NO RUN TESTS
	   * mvn clean install -DskipTests
	   
	### DOCKER
	    
	    * BUILD
	    $ docker build -t update-base-usebens .
	    
	    * RUN
	    $ docker run -it update-base-usebens
	
	### DOCKER COMPOSE
	
	    * BUILD more RUN
	    $ docker-compose up -d --build
 