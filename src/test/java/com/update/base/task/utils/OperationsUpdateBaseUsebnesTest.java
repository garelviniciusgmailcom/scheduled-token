package com.update.base.task.utils;
/*
import static org.junit.Assert.*;

import java.io.IOException;
import java.text.ParseException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.entities.repositories.services.services.BaseUsebensService;
import com.entities.repositories.services.services.StatisticsService;
import com.entities.repositories.services.utils.TmeZone;
import com.update.base.task.configurations.FtpProperties;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OperationsUpdateBaseUsebnesTest {
	
	@Autowired
	private FtpProperties ftpProperties;
	
	@Autowired
	private BaseUsebensService baseUsebensService;
	
	@Autowired
	private StatisticsService statisticsService;
	
	@Test
	public void testOperationUpdateBaseUsebens() throws ParseException, IOException, Exception {
		
		String startTime=TmeZone.getDataTodayTimeZoneBrasilSaoPaulo();
		
		new OperationUpdateBaseUsebens( null, this.baseUsebensService, startTime)
        .fileIsUpdatedByOperator()
          .cloneDataInTheFile()
          .removeAllDatasPreviousInTheDataBase()
          .saveAllNewData()
        .fileDoesNotUpdatedByOperator()
          .attackSleepTimeforFtpHost( ftpProperties.getSleepTime() )
        .statisticGeneratorAndSaveDataBase( this.statisticsService );
		
		assertTrue(true);
	}

}
*/