package com.update.base.task.utils;
/*
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.net.ftp.FTPClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.entities.repositories.services.dtos.BaseUsebensDto;
import com.entities.repositories.services.utils.CsvToObject;
import com.update.base.task.configurations.FtpProperties;
import com.update.base.task.ftp.FtpFace;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CsvToObjectTest {
	
	@Autowired
	private FtpProperties ftpProperties;
	
	private FtpFace ftpCon;
	
	private FTPClient cli;

	@Test
	public void testFileDateToday() throws NullPointerException, Exception {
		this.ftpCon = new FtpFace( this.ftpProperties.getHOSTNAME(), this.ftpProperties.getUSER(), this.ftpProperties.getPASSWORD(), this.ftpProperties.getFILENAME() );
		cli = ftpCon.returnCli();
		List<BaseUsebensDto> lst = new CsvToObject().beanBuilder(new InputStreamReader(cli.retrieveFileStream(cli.mlistFile(ftpProperties.getFILENAME()).getName())), BaseUsebensDto.class, ';')
//		List<BaseUsebensDto> lst = new CsvToObject().beanBuilder(new InputStreamReader( new FileInputStream( "datas/31 - Apolices Vigentes e Canceladas (Para Posicionamento).csv" ) ), BaseUsebensDto.class, ';')
	        .stream()
	        .filter(p -> p != null)
            .filter(p -> p.getStatus()!= null )
			.filter( c -> c.getStatus().equalsIgnoreCase("VIGENTE") || c.getStatus().equalsIgnoreCase("CANCELADA") )
			.collect( Collectors.toList() );
		System.out.println(
			lst			   
			  .stream()
				 .filter( p -> p.getPlaca().equalsIgnoreCase("CRG4119") )
				 .findFirst().get().toBaseUsebens().toMyString()
		);
	  System.out.println("Count: "+ lst.size());
//	  for(int i=0; i < 10; i++) System.out.println( lst.get(i).toString() );
	  assertTrue( lst.size()  > 0  );
	}
}
*/