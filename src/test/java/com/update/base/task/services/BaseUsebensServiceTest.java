package com.update.base.task.services;
/*
import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.junit4.SpringRunner;

import com.entities.repositories.services.documents.BaseUsebens;
import com.entities.repositories.services.services.BaseUsebensService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class BaseUsebensServiceTest {
	
	@Autowired
	private BaseUsebensService baseUsebensService;
	
	@Test
	public void testFindAll() {
		List<BaseUsebens> lst = this.baseUsebensService.findAll(0, 100, Direction.DESC).getContent();
		assertTrue( (lst.size() == 100 ) );
	}

	@Test
	public void testFindAllByPlaca() {
		List<BaseUsebens> lst = this.baseUsebensService.findByPlaca("DFS1793", 0, 100, Direction.DESC).getContent();
		assertTrue( (lst.size() > 0 ) );
	}

	@Test
	public void testFindByPlacaAndStatus() {
		Optional<BaseUsebens> opt = this.baseUsebensService.findByPlacaAndStatus("DFS1793", "CANCELADA");
		assertTrue( opt.isPresent() );
	}

	@Test
	public void testFindActiveBaseCount() {
		long count = this.baseUsebensService.findActiveBaseCount();
		assertTrue(count > 0);
	}

	@Test
	public void testFindCanceledBaseCount() {
		long count = this.baseUsebensService.findCanceledBaseCount();
		assertTrue( count > 0);
	}

	@Test
	public void testFindAllPartnerDistinctCount() {
		long count = this.baseUsebensService.findAllPartnerDistinctCount();
		assertTrue( count > 0);
	}

	@Test
	public void testFindAllCount() {
		long count = this.baseUsebensService.findAllCount();
		assertTrue( count > 0);
	}
	
//	@Test
//	public void testCloneCollectionBaseUsebens() {
//		this.baseUsebensService.cloneCollectionBaseUsebens();
//		assertTrue( true );
//	}
	
//	@Test
//	public void testDeleteAllCloneCollection() {
//		this.baseUsebensService.deleteAllCloneCollection();
//		assertTrue( true );
//	}
}
*/