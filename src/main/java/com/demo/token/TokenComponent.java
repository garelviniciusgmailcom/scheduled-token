package com.demo.token;

import com.demo.token.adapters.TokenService;
import com.demo.token.dtos.Token;
import com.sun.net.httpserver.Headers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Component
public class TokenComponent {

	private static final String KEY = "";

	private Token token;

	@Autowired
	private TokenService tokenService;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private DynamicSchedule dynamicSchedule;

	@Value("${url.authenticate.toke}")
	private String url;

	@Scheduled(fixedDelayString = "${schedule.token.delay}")
	private void scheduleToken() throws URISyntaxException {
		Headers headers = new Headers();
		headers.add("Key","Value");

		HttpEntity request = new HttpEntity(headers);

		ResponseEntity<Token> responseEntity = this.restTemplate.postForObject(new URI(url), request, ResponseEntity.class);

		if (responseEntity.getStatusCode().isError()) {
			throw new InternalError("Error: no get token.");
		}
		this.token = responseEntity.getBody();
		this.tokenService.save(KEY,this.token.toString());
		this.dynamicSchedule.increaseDelay(this.token.getTimeTokenRefresh());
	}

	public Token getToken(){
		return this.token;
	}

}
