package com.demo.token.adapters;

public interface TokenService {

    String findTokenByService(String service);

    String findTimeExpiredToken(String key);

    void save(String key, String value);
}
