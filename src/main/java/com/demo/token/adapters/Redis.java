package com.demo.token.adapters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

@Service
public class Redis implements TokenService {

    @Autowired
    private Jedis jedis;

    @Override
    public String findTokenByService(String hash) {
        return jedis.get(hash);
    }

    @Override
    public String findTimeExpiredToken(String key) {
        return jedis.get(key);
    }

    @Override
    public void save(String key, String value) {
        jedis.set(key,value);
    }
}
